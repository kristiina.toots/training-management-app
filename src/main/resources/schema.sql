DROP TABLE IF EXISTS `person_group`;
DROP TABLE IF EXISTS `person_training`;
DROP TABLE IF EXISTS `group_training_time`;
DROP TABLE IF EXISTS `person`;
DROP TABLE IF EXISTS `training`;
DROP TABLE IF EXISTS `group`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);


CREATE TABLE `group` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `group_name` VARCHAR(190) NOT NULL,
    `monthly_rate` INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `training` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `group_id` INT NOT NULL,
    `training_date` VARCHAR(15) NOT NULL,
    `comments` VARCHAR(500),
    PRIMARY KEY (id),
    CONSTRAINT `FK_training_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`)
);

CREATE TABLE `person` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(50) NOT NULL,
    `last_name` VARCHAR(50) NOT NULL,
    `personal_code` VARCHAR(50) NOT NULL,
    `phone` VARCHAR(50) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `comments` VARCHAR(500),
    PRIMARY KEY (id)
);

CREATE TABLE `person_training` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `person_id` INT NOT NULL,
    `training_id` INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_person_training_training` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`),
    CONSTRAINT `FK_person_training_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);

CREATE TABLE `person_group` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `group_id` INT NOT NULL,
    `person_id` INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_person_group_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
    CONSTRAINT `FK_person_group_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);

CREATE TABLE `group_training_time` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `day` VARCHAR (15) NOT NULL,
    `time` VARCHAR (15) NOT NULL,
    `group_id` INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_group_training_time_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`)
);

