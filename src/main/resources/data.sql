INSERT INTO `user` (username, password)
VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO `group` (group_name, monthly_rate) VALUES ('Young Pro-climbers', '70');
INSERT INTO `group` (group_name, monthly_rate) VALUES ('Adult Weekend Warriors', '50');

INSERT INTO training (training_date, comments, group_id) VALUES ('2020-05-11', 'training took place in Tartu', '1');
INSERT INTO training (training_date, comments, group_id) VALUES ('2020-05-13', 'Steffi was taken to hospital after a bad fall', '2');
INSERT INTO training (training_date, group_id) VALUES ('2020-05-15', '1');
INSERT INTO training (training_date, group_id) VALUES ('2020-05-18', '1');

insert into person (first_name, last_name, personal_code, phone, email) values ('Lyndel', 'Iczokvitz', '1795114991', '3375371943', 'liczokvitz0@nhs.uk');
insert into person (first_name, last_name, personal_code, phone, email) values ('Keenan', 'Coltman', '9002547862', '3556527675', 'kcoltman1@mac.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Steffi', 'Verrier', '5415096884', '6629292600', 'sverrier2@google.de');
insert into person (first_name, last_name, personal_code, phone, email) values ('Savina', 'Biernat', '5852329959', '3658070958', 'sbiernat3@umn.edu');
insert into person (first_name, last_name, personal_code, phone, email) values ('Major', 'Pahlsson', '3337575110', '8428685381', 'mpahlsson4@google.com.hk');
insert into person (first_name, last_name, personal_code, phone, email) values ('Elvyn', 'Snazle', '3350378617', '2102980062', 'esnazle5@craigslist.org');
insert into person (first_name, last_name, personal_code, phone, email) values ('Alexa', 'Marryatt', '5724989484', '5825254263', 'amarryatt6@4shared.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Lilyan', 'Costar', '8242335087', '8844932063', 'lcostar7@baidu.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Toni', 'Maytom', '4439262341', '1334560934', 'tmaytom8@deviantart.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Yancey', 'Lemerie', '1393878393', '6318193695', 'ylemerie9@businesswire.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Kora', 'Bright', '3010107196', '7851486135', 'kbrighta@va.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Bernardo', 'Brolechan', '4228326064', '2209102861', 'bbrolechanb@dagondesign.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Luella', 'Daniely', '4195206227', '1202742971', 'ldanielyc@blogspot.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Kinnie', 'Dunnet', '0349282072', '8149262382', 'kdunnetd@newyorker.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Robin', 'Sieb', '0110850289', '5083345485', 'rsiebe@wikispaces.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Stepha', 'Novak', '5249394329', '6061339902', 'snovakf@rakuten.co.jp');
insert into person (first_name, last_name, personal_code, phone, email) values ('Fannie', 'McNair', '1062873181', '4413920384', 'fmcnairg@nps.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Breena', 'Stiant', '5551142035', '4555522034', 'bstianth@flavors.me');
insert into person (first_name, last_name, personal_code, phone, email) values ('Marlin', 'Wickett', '3325267130', '9069777689', 'mwicketti@angelfire.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Shanda', 'Mitcheson', '0517760886', '5208534937', 'smitchesonj@unc.edu');
insert into person (first_name, last_name, personal_code, phone, email) values ('Nolly', 'Wikey', '9621147158', '8074027536', 'nwikeyk@theglobeandmail.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Khalil', 'Glander', '2628191970', '4277384222', 'kglanderl@t.co');
insert into person (first_name, last_name, personal_code, phone, email) values ('Marlow', 'Nanson', '3808337915', '4351377315', 'mnansonm@tmall.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Saundra', 'Belderfield', '8708411921', '7507006708', 'sbelderfieldn@weebly.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Coralyn', 'Minear', '8300258094', '8017808551', 'cminearo@census.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Hortense', 'Coger', '1139589776', '5681495312', 'hcogerp@ezinearticles.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Pace', 'Goudman', '5910346822', '3554825158', 'pgoudmanq@globo.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Jase', 'Hassell', '1592116825', '2146224389', 'jhassellr@fema.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Marylee', 'Cruikshanks', '8656344366', '8221967690', 'mcruikshankss@timesonline.co.uk');
insert into person (first_name, last_name, personal_code, phone, email) values ('Melodie', 'Beamish', '0374686645', '8935595776', 'mbeamisht@amazonaws.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Andrus', 'Zanitti', '6558672871', '2734575486', 'azanittiu@noaa.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Cirilo', 'Nielson', '7095261113', '6744586014', 'cnielsonv@is.gd');
insert into person (first_name, last_name, personal_code, phone, email) values ('Sayre', 'Adriani', '1066234361', '4078237911', 'sadrianiw@hud.gov');
insert into person (first_name, last_name, personal_code, phone, email) values ('Horatio', 'Drees', '9513755177', '5703608728', 'hdreesx@usnews.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Evelyn', 'Jepps', '7224174084', '2533557012', 'ejeppsy@oracle.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Lindy', 'Sharvill', '7207195702', '6079893913', 'lsharvillz@about.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Gavan', 'Snadden', '4357785286', '9046771407', 'gsnadden10@odnoklassniki.ru');
insert into person (first_name, last_name, personal_code, phone, email) values ('Madelena', 'Marron', '5514111393', '9443548993', 'mmarron11@1688.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Noak', 'Castellaccio', '7339238301', '1799654320', 'ncastellaccio12@mit.edu');
insert into person (first_name, last_name, personal_code, phone, email) values ('Genny', 'Kausche', '6593381101', '4215645024', 'gkausche13@networkadvertising.org');
insert into person (first_name, last_name, personal_code, phone, email) values ('Kennie', 'Gerring', '5013873991', '9135566235', 'kgerring14@weibo.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Adriena', 'Milier', '8951977536', '1108905792', 'amilier15@dagondesign.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Clovis', 'Burehill', '5849423117', '3829490612', 'cburehill16@buzzfeed.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Arne', 'Annion', '1497158230', '7807298941', 'aannion17@artisteer.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Diann', 'Klehyn', '9505163630', '9091821553', 'dklehyn18@harvard.edu');
insert into person (first_name, last_name, personal_code, phone, email) values ('Lannie', 'Stud', '7636607239', '4075228690', 'lstud19@latimes.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Juliet', 'Sapauton', '5467389300', '2449692461', 'jsapauton1a@icq.com');
insert into person (first_name, last_name, personal_code, phone, email) values ('Euphemia', 'Breese', '3471079661', '7721999894', 'ebreese1b@wisc.edu');
insert into person (first_name, last_name, personal_code, phone, email) values ('Cass', 'Paish', '7706315444', '8977895437', 'cpaish1c@earthlink.net');
insert into person (first_name, last_name, personal_code, phone, email) values ('Ernestus', 'Smale', '2808136110', '4799158652', 'esmale1d@mac.com');

INSERT INTO group_training_time (day, time, group_id) VALUES ('Monday', '11:00-13:00', '1');
INSERT INTO group_training_time (day, time, group_id) VALUES ('Wednesday', '16:00-18:00', '2');
INSERT INTO group_training_time (day, time, group_id) VALUES ('Friday', '12:00-14:00', '1');

-- `person_training` & `person_group`
-- pole vaja midagi ise lisada, sest tuletatakse teistest tabelitest?

