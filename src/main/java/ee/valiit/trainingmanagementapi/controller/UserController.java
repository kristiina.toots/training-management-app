package ee.valiit.trainingmanagementapi.controller;

import ee.valiit.trainingmanagementapi.dto.GenericResponseDto;
import ee.valiit.trainingmanagementapi.dto.JwtResponseDto;
import ee.valiit.trainingmanagementapi.dto.UserRegistrationDto;
import ee.valiit.trainingmanagementapi.dto.UsernamePasswordDto;
import ee.valiit.trainingmanagementapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody UsernamePasswordDto request) throws Exception {
        return userService.authenticate(request);
    }
}
