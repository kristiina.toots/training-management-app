package ee.valiit.trainingmanagementapi.controller;

import ee.valiit.trainingmanagementapi.model.OperationResult;
import ee.valiit.trainingmanagementapi.model.Training;
import ee.valiit.trainingmanagementapi.repository.TrainingsRepository;
import ee.valiit.trainingmanagementapi.service.TrainingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trainings")
@CrossOrigin("*")
public class TrainingsController {

  @Autowired
  private TrainingsRepository trainingsRepository;
  @Autowired
  private TrainingsService trainingsService;

  // get - all
  @GetMapping()
  public List<Training> getTrainings() {
    return trainingsRepository.getTrainings();
  }

  // Get - single training by id
  @GetMapping("/{trainingId}")
  public Training getSingleTraining(@PathVariable("trainingId") int id) {
    return trainingsRepository.getTraining(id);
  }

  // POST - add training
  @PostMapping("/add")
  public ResponseEntity<OperationResult> addTraining(@RequestBody Training training) {
    OperationResult result = trainingsService.addTraining(training);
    if (result.isSuccess()) {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
    } else {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
  }
}