package ee.valiit.trainingmanagementapi.controller;

import com.itextpdf.text.DocumentException;
import ee.valiit.trainingmanagementapi.service.FileService;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/files")
@CrossOrigin("*")
public class FileController {

  @Autowired
  private FileService fileService;

  @GetMapping("/pdf/{message}")
  public ResponseEntity<Resource> generatePdf(@PathVariable String message,
      HttpServletRequest request) throws IOException, DocumentException {
    Resource resource = fileService.generatePdf(message);
    String contentType = request.getServletContext()
        .getMimeType(resource.getFile().getAbsolutePath());
    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + resource.getFilename() + "\"")
        .body(resource);
  }

  @GetMapping("/pdf/{person_id}/{group_id}")
  public ResponseEntity<Resource> generatePdf(@PathVariable int person_id, @PathVariable int group_id,
      HttpServletRequest request) throws IOException, DocumentException {
    Resource resource = fileService.generateBillPdf(person_id, group_id);
    String contentType = request.getServletContext()
        .getMimeType(resource.getFile().getAbsolutePath());
    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + resource.getFilename() + "\"")
        .body(resource);
  }

}