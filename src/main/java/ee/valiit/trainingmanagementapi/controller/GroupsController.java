package ee.valiit.trainingmanagementapi.controller;

import ee.valiit.trainingmanagementapi.model.Group;
import ee.valiit.trainingmanagementapi.model.OperationResult;
import ee.valiit.trainingmanagementapi.model.Person;
import ee.valiit.trainingmanagementapi.repository.GroupsRepository;
import ee.valiit.trainingmanagementapi.service.GroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/groups")
@CrossOrigin("*")
public class GroupsController {

  @Autowired
  private GroupsRepository groupsRepository;

  @Autowired
  private GroupsService groupsService;

  // get - all groups
  @GetMapping("/all")
  public List<Group> getGroups() {
    return groupsRepository.getGroups();
  }

  // Get - single group by id
  @GetMapping("/{groupId}")
  public Group getSingleGroup(@PathVariable("groupId") int id) {
    return groupsRepository.getGroup(id);
  }

  // get - all members in database
  @GetMapping("/allMembers")
  public List<Person> getAllMembers() {
    return groupsRepository.getAllMembers();
  }


  // PUT - update group
  @PutMapping("/update")
  public ResponseEntity<OperationResult> updateGroup(@RequestBody Group group){
    OperationResult result = groupsService.updateGroup(group);
    if (result.isSuccess()) {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
  }

  // POST - add group
  @PostMapping("/add")
  public ResponseEntity<OperationResult> addGroup(@RequestBody Group group) {
    OperationResult result = groupsService.addNewGroup(group);
    if (result.isSuccess()) {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
    } else {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
  }

  // DELETE - delete single group by
  @DeleteMapping("/{groupId}")
  public ResponseEntity<OperationResult> deleteGroup(@PathVariable("groupId") int id) {
    OperationResult result = groupsService.deleteGroup(id);
    if (result.isSuccess()) {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
  }

}
