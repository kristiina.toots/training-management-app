package ee.valiit.trainingmanagementapi.repository;

import ee.valiit.trainingmanagementapi.model.Group;
import ee.valiit.trainingmanagementapi.model.Person;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupsRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private TrainingsRepository trainingRepository;

  public List<Group> getGroups() {
    return jdbcTemplate.query(
        "select * from `group` where (is_deleted != 1 or is_deleted is null)",
        (row, number) -> new Group(
            row.getInt("id"),
            row.getString("group_name"),
            row.getInt("monthly_rate"),
            this.getGroupPersons(row.getInt("id")),
            trainingRepository.getGroupTrainings(row.getInt("id")),
            Collections.emptyList()
        )
    );
  }

  public Group getGroup(int id) {
    List<Group> groups = jdbcTemplate.query(
        "select * from `group` where `id` = ?",
        new Object[]{id},
        (row, number) -> new Group(
            row.getInt("id"),
            row.getString("group_name"),
            row.getInt("monthly_rate"),
            this.getGroupPersons(id),
            trainingRepository.getGroupTrainings(id),
            Collections.emptyList()
        )
    );
    return groups.size() > 0 ? groups.get(0) : null;

  }


  public List<Person>   getGroupPersons(int groupId){
    return jdbcTemplate.query(
        "select * from person p inner join person_group pg on pg.person_id = p.id where pg.group_id = ?",
        new Object[]{groupId},
        (row, number) -> {
          return new Person(
              row.getInt("id"),
              row.getString("first_name"),
              row.getString("last_name"),
              row.getString("personal_code"),
              row.getString("phone"),
              row.getString("email"),
              row.getString("comments")
              );
        }
    );
  }


  public List<Person> getAllMembers(){
    return jdbcTemplate.query(
        "select * from person",
        new Object[]{},
        (row, number) -> {
          return new Person(
              row.getInt("id"),
              row.getString("first_name"),
              row.getString("last_name"),
              row.getString("personal_code"),
              row.getString("phone"),
              row.getString("email"),
              row.getString("comments")
          );
        }
    );
  }


  public int addGroup(Group group) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(
          "insert into `group` (`group_name`, `monthly_rate`) values (?, ?)",
          Statement.RETURN_GENERATED_KEYS);
      ps.setString(1, group.getGroup_name());
      ps.setInt(2, group.getMonthly_rate());
      return ps;
    }, keyHolder);
    return keyHolder.getKey().intValue();
  }


  public void addMemberToGroup(int group_id, int member_id) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(
          "insert into person_group (`group_id`, `person_id`) values (?, ?)",
          Statement.RETURN_GENERATED_KEYS);
      ps.setInt(1, group_id);
      ps.setInt(2, member_id);
      return ps;
    }, keyHolder);
  }

  public void updateGroup(Group group) {
    jdbcTemplate.update(
        "update `group` set `group_name` = ?, `monthly_rate` = ? where `id` = ?",
        group.getGroup_name(), group.getMonthly_rate(), group.getId()
    );
  }

  public void deleteGroup(int groupId) {
    jdbcTemplate.update("update `group` set is_deleted = 1 where id = ?", groupId);
  }

  public boolean groupExists(int groupId) {
    Integer count = jdbcTemplate.queryForObject(
        "select count(*) from `group` where id = ?",
        new Object[] { groupId },
        Integer.class
    );
    return count != null && count > 0;
  }

  public Person getPerson(int id) {
    List<Person> persons = jdbcTemplate.query(
        "select * from `person` where `id` = ?",
        new Object[]{id},
        (row, number) -> new Person(
            row.getInt("id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getString("personal_code"),
            row.getString("phone"),
            row.getString("email"),
            row.getString("comments")
        )
    );
    return persons.size() > 0 ? persons.get(0) : null;
  }


}
