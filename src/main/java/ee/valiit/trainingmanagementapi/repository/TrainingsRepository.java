package ee.valiit.trainingmanagementapi.repository;

import ee.valiit.trainingmanagementapi.model.Person;
import ee.valiit.trainingmanagementapi.model.Training;
import java.sql.PreparedStatement;
import java.sql.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TrainingsRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;


  public List<Training> getTrainings() {
    return jdbcTemplate.query("select * from `training`", (row, number) ->
    {
      return new Training(
          row.getInt("id"),
          row.getInt("group_id"),
          row.getString("training_date"),
          row.getString("comments"),
          null,
          getPersonsByTraining(row.getInt("id"))
      );
    });
  }

  public List<Training> getGroupTrainings(int groupId){
    return jdbcTemplate.query(
        "select * from `training` where `group_id` = ?",
        new Object[]{groupId},
        (row, number) -> {
          return new Training(
              row.getInt("id"),
              row.getInt("group_id"),
              row.getString("training_date"),
              row.getString("comments"),
              null,
              getPersonsByTraining(row.getInt("id"))
          );
        }
    );
  }

  public List<Person> getPersonsByTraining(int trainingId) {
    return jdbcTemplate.query(
        "SELECT person.* FROM person inner join person_training on person.id = person_training.person_id where person_training.training_id = ?",
        new Object[]{trainingId},
        (row, number) -> new Person(
            row.getInt("id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getString("personal_code"),
            row.getString("phone"),
            row.getString("email"),
            row.getString("comments")
        )
    );
  }

  public Training getTraining(int trainingId) {
    List<Training> trainings = jdbcTemplate.query(
        "select * from training where id = ?",
        new Object[]{trainingId},
        (row, number) -> {
          return new Training(
              row.getInt("id"),
              row.getInt("group_id"),
              row.getString("training_date"),
              row.getString("comments"),
              null,
              getPersonsByTraining(trainingId)
          );
        });
    return trainings.size() > 0 ? trainings.get(0) : null;

  }

  public int addTraining(Training training) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(
          "insert into training (`group_id`, `training_date`, `comments`) values (?, ?, ?)",
          Statement.RETURN_GENERATED_KEYS);
      ps.setInt(1, training.getGroup_id());
      ps.setString(2, training.getTraining_date());
      ps.setString(3, training.getComments());
      return ps;
    }, keyHolder);
    return keyHolder.getKey().intValue();
  }

  public void addMemberToTraining(int training_id, int member_id) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(
          "insert into person_training (`person_id`, `training_id`) values (?, ?)",
          Statement.RETURN_GENERATED_KEYS);
      ps.setInt(1, member_id);
      ps.setInt(2, training_id);
      return ps;
    }, keyHolder);
  }
}




