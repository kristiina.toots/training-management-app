package ee.valiit.trainingmanagementapi.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import ee.valiit.trainingmanagementapi.model.Group;
import ee.valiit.trainingmanagementapi.model.Person;
import ee.valiit.trainingmanagementapi.repository.GroupsRepository;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;

@Service
public class FileService {

  @Autowired
  private GroupsRepository groupsRepository;

  @Value("${file.upload-dir}")
  private String uploadDir;

  private Path getUploadDir() {
    return Paths.get(this.uploadDir).toAbsolutePath().normalize();
  }

  public String storeFile(MultipartFile file) throws IOException {
    String fileName = generateFileName(file);
    Path targetLocation = this.getUploadDir().resolve(fileName);
    Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
    return fileName;
  }

  public Resource loadFileAsResource(String fileName) throws MalformedURLException {
    Path filePath = this.getUploadDir().resolve(fileName).normalize();
    return new UrlResource(filePath.toUri());
  }

  private String generateFileName(MultipartFile inputFile) {
    String fileName = StringUtils.cleanPath(inputFile.getOriginalFilename());
    String extension = "";
    int i = fileName.lastIndexOf(".");
    if (i > 0) {
      extension = fileName.substring(i);
    }
    return UUID.randomUUID().toString() + extension;
  }

  private String generatePdfFileName() {
    return UUID.randomUUID().toString() + ".pdf";
  }

  public Resource generatePdf(String message) throws FileNotFoundException, DocumentException, MalformedURLException {
    Document document = new Document();
    String pdfFileName = this.generatePdfFileName();
    String pdfFilePath = this.getUploadDir() + "/" + pdfFileName;
    PdfWriter.getInstance(document, new FileOutputStream(pdfFilePath));

    document.open();
    Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
    Chunk chunk = new Chunk(message, font);

    document.add(chunk);
    document.close();

    return this.loadFileAsResource(pdfFileName);
  }

  public Resource generateBillPdf(int person_id, int group_id) throws FileNotFoundException, DocumentException, MalformedURLException {
    Document document = new Document();

    String pdfFileName = this.generatePdfFileName();
    String pdfFilePath = this.getUploadDir() + "/" + pdfFileName;
    PdfWriter.getInstance(document, new FileOutputStream(pdfFilePath));

    document.open();
    //get person details
    Person person = groupsRepository.getPerson(person_id);
    Group group = groupsRepository.getGroup(group_id);
    String companyName = "Firmanimi OÜ";
    String companyAddress = "Kase tee 2\nHarjumaa\n63274";
    String companyDetails = "+372 5843 1354\ninfo@firmanimi.ee\nReg nr: 5403921123";
    String companyBankDetails = "LHV\nEE667700771003313451";
    String name = "Kristiina Bananahammock";
    String nameTitle = "Juhataja";
    Paragraph title = new Paragraph(companyName, new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD));
    addEmptyLine(title, 3);
    document.add(title);

    // siia tuleb hiljem unikaalne arve number
    LocalDate dateToday = LocalDate.now();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyMMdd");
    LocalDateTime now = LocalDateTime.now();
    Calendar cal = Calendar.getInstance();
    String month = new SimpleDateFormat("MMMM").format(cal.getTime());
//    System.out.println(dtf.format(now));
//    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    int billNumber = person.getId();
    Paragraph billDetails = new Paragraph("Arve nr: "+ billNumber + dtf.format(now));

    Paragraph date = new Paragraph("Tänane kuupäev: " + dateToday);
    LocalDate futureDate = LocalDate.now().plusMonths(1);
    Paragraph datePlusMonth = new Paragraph("Makse tähtaeg: " + futureDate);
    addEmptyLine(datePlusMonth, 2);

    document.add(billDetails);
    document.add(date);
    document.add(datePlusMonth);

    Paragraph contactDetails = new Paragraph("Maksja: " + "\n" + person.getFirst_name() + " " + person.getLast_name() +
        "\n" + person.getEmail() + "\n" + person.getPhone());

    addEmptyLine(contactDetails, 4);
    document.add(contactDetails);

    // create a table

    PdfPTable table = new PdfPTable(new float[] { 2, 1, 1, 1, 1 });

    PdfPCell c1 = new PdfPCell(new Phrase("Teenus"));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    c1 = new PdfPCell(new Phrase("Ühik"));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    c1 = new PdfPCell(new Phrase("Kogus"));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);
    table.setHeaderRows(1);

    c1 = new PdfPCell(new Phrase("Hind"));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    c1 = new PdfPCell(new Phrase("Summa"));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);
    table.setHeaderRows(1);

    table.addCell("Ronimistrennid:\n" + group.getGroup_name());
    table.addCell(month);
    table.addCell("1");
    table.addCell(group.getMonthly_rate() + " EUR");
    table.addCell(group.getMonthly_rate() + " EUR");

    document.add(table);

    Paragraph total = new Paragraph("KOKKU: " + group.getMonthly_rate() + " EUR", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD));
    addEmptyLine(total, 7);
    total.setAlignment(2);
    document.add(total);
    Paragraph info = new Paragraph(companyName + " ei ole käibemaksukohuslane.", new Font(Font.FontFamily.TIMES_ROMAN, 10));
    addEmptyLine(info, 4);
    document.add(info);
    Paragraph signature = new Paragraph(name + "\n" + nameTitle);
    addEmptyLine(signature, 4);
    document.add(signature);

    PdfPTable table2 = new PdfPTable(3);

    PdfPCell c2 = new PdfPCell(new Phrase(companyName + "\n" + companyAddress, new Font(Font.FontFamily.TIMES_ROMAN, 8)));
    c2.setHorizontalAlignment(Element.ALIGN_CENTER);
    table2.addCell(c2);

    c2 = new PdfPCell(new Phrase(companyDetails, new Font(Font.FontFamily.TIMES_ROMAN, 10)));
    c2.setHorizontalAlignment(Element.ALIGN_CENTER);
    table2.addCell(c2);

    c2 = new PdfPCell(new Phrase(companyBankDetails, new Font(Font.FontFamily.TIMES_ROMAN, 10)));
    c2.setHorizontalAlignment(Element.ALIGN_CENTER);
    table2.addCell(c2);

    document.add(table2);

    document.close();

    return this.loadFileAsResource(pdfFileName);
  }

  private static void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
      paragraph.add(new Paragraph(" "));
    }
  }

}
