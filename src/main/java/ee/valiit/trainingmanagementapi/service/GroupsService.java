package ee.valiit.trainingmanagementapi.service;

import ee.valiit.trainingmanagementapi.model.Group;
import ee.valiit.trainingmanagementapi.model.OperationResult;
import ee.valiit.trainingmanagementapi.repository.GroupsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class GroupsService {

  @Autowired
  private GroupsRepository groupsRepository;

  public OperationResult addGroup(Group group) {
    int groupId = groupsRepository.addGroup(group);
    return new OperationResult(true, "OK", groupId);
  }


  public OperationResult addNewGroup(Group group) {

    int groupId = groupsRepository.addGroup(group);
    for (int i = 0; i < group.getMember_ids().size(); i++) {
      groupsRepository.addMemberToGroup(groupId, group.getMember_ids().get(i));
    }
    return new OperationResult(true, "OK", groupId);

  }

  public OperationResult updateGroup(Group group) {
    try {
      Assert.isTrue(group != null, "Group data does not exist!");
      Assert.isTrue(group.getGroup_name() != null && !group.getGroup_name().equals(""), "Group name not specified!");
      Assert.isTrue(group.getMonthly_rate() > 0,
          "Monthly fee has to be specified!");
      groupsRepository.updateGroup(group);
      return new OperationResult(true, "OK", group.getId());
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      String message = e.getMessage();
      return new OperationResult(false, message, null);
    }
  }

  public OperationResult deleteGroup(int groupId) {
    try {
      Assert.isTrue(groupId > 0, "Group ID not specified!");
      Assert.isTrue(groupsRepository.groupExists(groupId), "The specified group does not exits!");
      groupsRepository.deleteGroup(groupId);
      return new OperationResult(true, "OK", groupId);
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      String message = e.getMessage();
      return new OperationResult(false, message, null);
    }
  }

}
