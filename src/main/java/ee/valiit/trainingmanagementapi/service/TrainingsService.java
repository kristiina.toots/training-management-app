package ee.valiit.trainingmanagementapi.service;

import ee.valiit.trainingmanagementapi.model.OperationResult;
import ee.valiit.trainingmanagementapi.model.Training;
import ee.valiit.trainingmanagementapi.repository.TrainingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingsService {

  @Autowired
  private TrainingsRepository trainingsRepository;

  public OperationResult addTraining(Training training) {
    //happy case - lisada treeningtabelisse kirje
    int trainingId = trainingsRepository.addTraining(training);
    for (int i = 0; i < training.getParticipatedMembers().size(); i++) {
      trainingsRepository.addMemberToTraining(trainingId, training.getParticipatedMembers().get(i));
    }
    return new OperationResult(true, "OK", trainingId);

  }
}
