package ee.valiit.trainingmanagementapi.model;

public class Person {
  private int id;
  private String first_name;
  private String last_name;
  private String personal_code;
  private String phone;
  private String email;
  private String comments;

  public Person(int id, String first_name, String last_name, String personal_code,
      String phone, String email, String comments) {
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.personal_code = personal_code;
    this.phone = phone;
    this.email = email;
    this.comments = comments;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public String getPersonal_code() {
    return personal_code;
  }

  public void setPersonal_code(String personal_code) {
    this.personal_code = personal_code;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @Override
  public String toString() {
    return "Person{" +
        "id=" + id +
        ", first_name='" + first_name + '\'' +
        ", last_name='" + last_name + '\'' +
        ", personal_code=" + personal_code +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        ", comments='" + comments + '\'' +
        '}';
  }
}
