package ee.valiit.trainingmanagementapi.model;

import java.util.Collections;
import java.util.List;

public class Group {
  private int id;
  private String group_name;
  private int monthly_rate;
  private List<Person> members;
  private List<Training> trainings = Collections.emptyList();
  private List<Integer> member_ids = Collections.emptyList();

  public Group() {
  }

  public Group(int id, String group_name, int monthly_rate,
      List<Person> members, List<Training> trainings, List<Integer> member_ids) {
    this.id = id;
    this.group_name = group_name;
    this.monthly_rate = monthly_rate;
    this.members = members;
    this.trainings = trainings;
    this.member_ids = member_ids;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getGroup_name() {
    return group_name;
  }

  public void setGroup_name(String group_name) {
    this.group_name = group_name;
  }

  public int getMonthly_rate() {
    return monthly_rate;
  }

  public void setMonthly_rate(int monthly_rate) {
    this.monthly_rate = monthly_rate;
  }

  public List<Person> getMembers() {
    return members;
  }

  public void setMembers(List<Person> members) {
    this.members = members;
  }

  public List<Training> getTrainings() {
    return trainings;
  }

  public void setTrainings(List<Training> trainings) {
    this.trainings = trainings;
  }

  public List<Integer> getMember_ids() {
    return member_ids;
  }

  public void setMember_ids(List<Integer> member_ids) {
    this.member_ids = member_ids;
  }

  @Override
  public String toString() {
    return "Group{" +
        "id=" + id +
        ", group_name='" + group_name + '\'' +
        ", monthly_rate=" + monthly_rate +
        ", members=" + members +
        ", trainings=" + trainings +
        ", member_ids=" + member_ids +
        '}';
  }
}
