package ee.valiit.trainingmanagementapi.model;

import java.util.List;

public class Training {
  private int id;
  private int group_id;
  private String training_date;
  private String comments;
  private List<Integer> participatedMembers;
  private List<Person> participants;

  public Training() {
  }

  public Training(int id, int group_id, String training_date, String comments,
      List<Integer> participatedMembers,
      List<Person> participants) {
    this.id = id;
    this.group_id = group_id;
    this.training_date = training_date;
    this.comments = comments;
    this.participatedMembers = participatedMembers;
    this.participants = participants;
  }

  public List<Person> getParticipants() {
    return participants;
  }

  public void setParticipants(List<Person> participants) {
    this.participants = participants;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getGroup_id() {
    return group_id;
  }

  public void setGroup_id(int group_id) {
    this.group_id = group_id;
  }

  public String getTraining_date() {
    return training_date;
  }

  public void setTraining_date(String training_date) {
    this.training_date = training_date;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public List<Integer> getParticipatedMembers() {
    return participatedMembers;
  }

  public void setParticipatedMembers(List<Integer> participatedMembers) {
    this.participatedMembers = participatedMembers;
  }

  @Override
  public String toString() {
    return "Training{" +
        "id=" + id +
        ", group_id=" + group_id +
        ", training_date='" + training_date + '\'' +
        ", comments='" + comments + '\'' +
        ", participatedMembers=" + participatedMembers +
        '}';
  }
}
